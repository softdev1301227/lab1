/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.chaiwat.lab1;

import java.util.Scanner;

/**
 *
 * @author Chaiwat
 */
public class Lab1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int round = 0;
        String Plyerturn = " ";

        char[][] board = {{'-','-','-'},
                          {'-','-','-'},
                          {'-','-','-'}};
        
        PrintBoard(board); 
        
        while(true){
            Xturn(Plyerturn, sc, board);
            round += 1;
            if(isGameFinished(board,round)){
                PrintBoard(board);
                break;
            }
            PrintBoard(board);
            System.out.println("round : " + round);
            Oturn(Plyerturn, sc, board);
            round += 1;
            if(isGameFinished(board,round)){
                PrintBoard(board);
                break;
            }
            PrintBoard(board);
            System.out.println("round : " + round);
        }
        
        System.out.println("Play again? (y/n):");
        String nextorder = sc.next();

        System.out.println("_______________________");
        
        while(nextorder.equals("y")){
            round = 0;
            Plyerturn = " ";
            
            char[][] board1 = {{'-','-','-'},
                          {'-','-','-'},
                          {'-','-','-'}};
        
        PrintBoard(board1); 
        
        while(true){
            Xturn(Plyerturn, sc, board1);
            round += 1;
            if(isGameFinished(board1,round)){
                PrintBoard(board1);
                break;
            }
            PrintBoard(board1);
            System.out.println("round : " + round);
            Oturn(Plyerturn, sc, board1);
            round += 1;
            if(isGameFinished(board1,round)){
                PrintBoard(board1);
                break;
            }
            PrintBoard(board1);
              System.out.println("round : " + round);
        }
        
        System.out.println("Play again? (y/n):");
        nextorder = sc.next();
        
        System.out.println("_______________________");
        }

    }

    private static void Xturn(String Plyerturn, Scanner sc, char[][] board) {
        Plyerturn = "X";
        System.out.println(Plyerturn + " : Turn");
        System.out.println("Please input row,col:");
        
        int row = sc.nextInt();
        int col = sc.nextInt();
        
        while(board[row - 1][col - 1] != '-'){
            System.out.println("Please input row,col agin:");
            row = sc.nextInt();
            col = sc.nextInt();
        }
        board[row - 1][col - 1] = 'X';
        System.out.println("_______________________");
    }

    private static void PrintBoard(char[][] board) {
        System.out.println(board[0][0] + " " + board[0][1] + " " + board[0][2]);
        System.out.println(board[1][0] + " " + board[1][1] + " " + board[1][2]);
        System.out.println(board[2][0] + " " + board[2][1] + " " + board[2][2]);
    }

    private static boolean isGameFinished(char[][] board,int round) {
        
        if(round == 9){
            System.out.println("Draw!!!");
            return true;
        }
        if(
           // win horizontal X
           (board[0][0] == 'X' && board[0][1] == 'X' && board [0][2] == 'X') ||
           (board[1][0] == 'X' && board[1][1] == 'X' && board [1][2] == 'X') ||
           (board[2][0] == 'X' && board[2][1] == 'X' && board [2][2] == 'X') ||
           // win vertical X
           (board[0][0] == 'X' && board[1][0] == 'X' && board [2][0] == 'X') ||
           (board[0][1] == 'X' && board[1][1] == 'X' && board [2][1] == 'X') ||
           (board[0][2] == 'X' && board[1][2] == 'X' && board [2][2] == 'X') ||
           // win oblique X     
           (board[0][0] == 'X' && board[1][1] == 'X' && board [2][2] == 'X') ||
           (board[0][2] == 'X' && board[1][1] == 'X' && board [2][0] == 'X')){
            System.out.println("Player X WIN!!!");
            return true;
        } if(
           // win horizontal O
           (board[0][0] == 'O' && board[0][1] == 'O' && board [0][2] == 'O') ||
           (board[1][0] == 'O' && board[1][1] == 'O' && board [1][2] == 'O') ||
           (board[2][0] == 'O' && board[2][1] == 'O' && board [2][2] == 'O') ||
           // win vertical O
           (board[0][0] == 'O' && board[1][0] == 'O' && board [2][0] == 'O') ||
           (board[0][1] == 'O' && board[1][1] == 'O' && board [2][1] == 'O') ||
           (board[0][2] == 'O' && board[1][2] == 'O' && board [2][2] == 'O') ||
           // win oblique O     
           (board[0][0] == 'O' && board[1][1] == 'O' && board [2][2] == 'O') ||
           (board[0][2] == 'O' && board[1][1] == 'O' && board [2][0] == 'O')){
            System.out.println("Player O WIN!!!");
            return true;
        }else{
            return false;
        }
    }

    private static void Oturn(String Plyerturn, Scanner sc, char[][] board) {
        Plyerturn = "O";
        System.out.println(Plyerturn + " : Turn");
        System.out.println("Please input row,col:");
        
        int row = sc.nextInt();
        int col = sc.nextInt();
        
        
        while(board[row - 1][col - 1] != '-'){
            System.out.println("Please input row,col agin:");
            row = sc.nextInt();
            col = sc.nextInt();
        }
        board[row - 1][col - 1] = 'O';
        System.out.println("_______________________");
    }
}
